package Sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class FahrenheitTest {

	@Test
	public void testConvertFromCelsius() {
		int t= Fahrenheit.convertFromCelsius(30);
		Assert.assertEquals("Test",86,t);
	}
	
	@Test (expected = RuntimeException.class)
	public void testConvertFromCelsiusException() {
		int t= Fahrenheit.convertFromCelsius(-10);
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		int t = Fahrenheit.convertFromCelsius((int) Math.round(25.2));
		Assert.assertEquals(77, t);
	}
	
	@Test 
	public void testConvertFromCelsiusBoundaryOut() {
		int t = Fahrenheit.convertFromCelsius((int) Math.round(25.8));
		Assert.assertEquals(78, t);
	}

}
