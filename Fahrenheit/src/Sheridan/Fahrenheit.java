package Sheridan;

public class Fahrenheit {
	
	public static int convertFromCelsius(int temp) {
		
		if(temp <0) {
			throw new RuntimeException("Negative Temperature"); 
		}
		else {
		return ((9*temp)/5 + 32);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Temperature in Fahrenheit is "+convertFromCelsius(30));
	}

}
